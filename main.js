window.onload = () => {

}
triggerAd = (type) => {
    // adPlayer = jwplayer("jw-test").setup({
    //     "playlist":"https://cdn.jwplayer.com/v2/media/1g8jjku3",
    //     "autostart":"viewable",
    //     "advertising": {
    //       "client": "vast",
    //       "schedule": [
    //         {
    //           "tag": "http://server.ideastudios.ro/sandbox/html5/wip/jw/"+type+".xml",
    //         }
    //       ]
    //     }
    //   });
    var adtag = "http://localhost:3000/gamebid-ads-jw/" + type + ".xml"
    if (type == "google_3")
        adtag = "https://pubads.g.doubleclick.net/gampad/ads?sz=480x70&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dnonlinear&correlator=" + Math.floor(Math.random() * 100000)
    if (type == 'google_linear')
        adtag = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=" + Math.floor(Math.random() * 100000)
    adPlayer = jwplayer('jw-test').setup({
        width: "100%",
        advertising: {
            outstream: true,
            endstate: "close",
            adscheduleid: "sample01",
            autoplayadsmuted: true,
            client: "vast",
            skipoffset: 10,
            skipmessage: 'Skip this ad in XX',
            minSuggestedDuration: 30,
            type: "nonlinear",
            schedule: {
                "adbreak-preroll": {
                    offset: "pre",
                    tag: adtag,
                    type: "nonlinear",
                }
            }
        }
    });

    adPlayer.on('adRequest', function(event) {
        console.log('AD REQUESTED', event);
    });
    adPlayer.on('adError', function(event) {
        console.log('AD ERROR', event);
    });
    adPlayer.on('adStarted', function(event) {
        console.log('AD STARTED', event);
    });
    adPlayer.on('adImpression', function(event) {
        console.log('AD IMPRESSION', event);
    });
    adPlayer.on('adSkipped', function(event) {
        console.log('AD SKIPPED', event);
    });
    adPlayer.on('adComplete', function(event) {
        console.log('AD COMPLETE', event);
    });
    adPlayer.on('adClick', function(event) {
        console.log('AD CLICK', event);
    });
}

initListeners = () => {}